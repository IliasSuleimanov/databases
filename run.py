from lxml import html
from lxml.html import HtmlElement

import page_parser
import xml_builder

try:
  from lxml import etree
  print("running with lxml.etree")
except ImportError:
  try:
    # Python 2.5
    import xml.etree.cElementTree as etree
    print("running with cElementTree on Python 2.5+")
  except ImportError:
    try:
      # Python 2.5
      import xml.etree.ElementTree as etree
      print("running with ElementTree on Python 2.5+")
    except ImportError:
      try:
        # normal cElementTree install
        import cElementTree as etree
        print("running with cElementTree")
      except ImportError:
        try:
          # normal ElementTree install
          import elementtree.ElementTree as etree
          print("running with ElementTree")
        except ImportError:
          print("Failed to import ElementTree from any known place")

import requests

if __name__ == "__main__":
    parser = page_parser.Parser("http://xsport.ua/")
    # task 1
    parser.get_all_text()
    # task 2
    parser.get_all_images()
    parser.save_all_in_html_file("file.html")
    # parser.print_urls() #second task - withdraw all links
    # task 3, 4
    parser2 = page_parser.Parser("http://www.meblium.com.ua/myagkaya-mebel/divany")
    parser2.get_products_information()