import xml_builder
import re
from lxml import html
from lxml.html import HtmlElement
import requests


class Parser(object):

    def __init__(self, url):
        self.images_paths = []
        self.text = []
        self.builder = xml_builder.Builder(True)
        self.home_url = url.split("/")[2]# http:/[0] / [1] xsport.ua [2] / [3]
        self.root_url = url
        self.page = requests.get(url)
        self.tree = html.fromstring(self.page.content)
        self.visited_pages = []
        self.desc = ""

    def get_all_text(self, string_array = False):

        current_text = self.tree.xpath("//*[text() and name()!='script']")
        for element in current_text:
            self.text.append(element.text)

        return self.text

    def get_all_images(self):
        if self.images_paths.__len__() == 0:
            self.images_paths = self.tree.xpath("//img[@src]/@src")
        # formatting, from local url of image on page, in global url
        for i in range(self.images_paths.__len__()):
            # [method for method in dir(self.images_paths[i]) if callable(getattr(self.images_paths[i], method))]
            # method.format()
            if self.images_paths[i][0:4] != "http":
                self.images_paths[i] = self.page.url + self.images_paths[i]
                # print self.images_paths[i]
            self.images_paths[i] = str(self.images_paths[i])

        return self.images_paths

    def save_all_in_file(self, filename):
        file = open(filename, 'w')
        for el in self.text:
            self.builder.add_fragment(el, attr="text")

        for el in self.images_paths:
            self.builder.add_fragment(el, attr="image")

        file.write(self.builder.get_tree())
        file.close()

    def save_all_in_html_file(self, filename):
        file = open(filename, 'w')
        for fragment in self.text:
            self.builder.add_fragment(fragment, attr="text", name="p")

        for fragment in self.images_paths:
            self.builder.add_fragment(fragment, name="img")

        file.write(self.builder.get_tree())
        file.close()

    def scan_pages(self, n):
        list_of_pages = self.tree.xpath("//a[@href]/@href")
        self.scan_current_page()
        i = 0
        cnt = 0
        while cnt != n:
            if str(list_of_pages[i]).__contains__(":"):
                i += 1
                continue
            try:
                value = list_of_pages[i].split("/")[2]
            except IndexError:
                print "Out of range"
                print list_of_pages[i]
                print list_of_pages[i].split("/")
            if list_of_pages[i][0:4] != "http" or list_of_pages[i][0:4] == "https":
                list_of_pages[i] = "http://{0}{1}".format(self.home_url, list_of_pages[i])
            if self.is_visited(list_of_pages[i]) or list_of_pages[i] == "" \
                    or list_of_pages[i].split("/")[2] != self.home_url:
                i += 1
                continue

            self.set_next_page(list_of_pages[i])
            self.scan_current_page()
            cnt += 1
        self.save_all_in_html_file("file.html")

    def scan_current_page(self):
        self.get_all_text()
        self.get_all_images()
        self.visited_pages.append(self.page.url)

    def is_visited(self, page_url):
        if page_url[-1] != "/":
            page_url+="/"
        return self.visited_pages.__contains__(page_url)

    def set_next_page(self, page_url):
        print page_url
        self.page = requests.get(page_url)
        self.tree = html.fromstring(self.page.content)

    def print_urls(self):
        links = self.tree.xpath("//a[@href]/@href")
        for link in links:
            if link[0:4] != "http":
                print "http://" + self.home_url + link
            else:
                print link

    def get_products_information(self):
        navigation_links = self.tree.xpath("//a[@class='navig pagenav']/@href")
        navig_counter = 0
        i = 0
        n = 20
        for link in navigation_links:
            links = self.tree.xpath("//a[@class='product-link']/@href")
            for href in links:
                if self.visited_pages.__contains__(href):
                    continue
                self.page = requests.get(href)
                self.desc = requests.get(href + "#params")
                self.get_information_from_current_page()
                self.visited_pages.append(self.page.url)
                i+=1
                if i == n:
                    break
            if i == n:
                break
            self.page = requests.get(link)
            navig_counter+=1

        self.builder.write_in_file()
        self.builder.xhtml_file.close()


    def get_information_from_current_page(self):
        self.tree = html.fromstring(self.page.content)

        price = self.tree.xpath("//span[@class='new-price']/text()")
        print price[0] + " price"
        data = list()
        self.tree = html.fromstring(self.desc.content)

        # file = open("file.html", 'w')

        lines = self.tree.xpath("//tr")

        for line in lines:
            data.append([row[0].text for row in line])

        img_source = self.tree.xpath("//img[@class='img-responsive']/@src")

        # #[file.write("<img src=\"" + img + "\"/>") for img in img_source]
        self.builder.add_new_product(self.page.url, price[0], lines, img_source[0])
        # self.format_product_params_in_xhtml(price, img_source[0], lines)
        # for i in range(data.__len__()):
            # print data[i][0]
        # # file.close()

    def format_product_params_in_xhtml(self, price, img_source, table):
        print
