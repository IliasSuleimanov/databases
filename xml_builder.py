try:
  from lxml import etree
  print("running with lxml.etree")
except ImportError:
  try:
    # Python 2.5
    import xml.etree.cElementTree as etree
    print("running with cElementTree on Python 2.5+")
  except ImportError:
    try:
      # Python 2.5
      import xml.etree.ElementTree as etree
      print("running with ElementTree on Python 2.5+")
    except ImportError:
      try:
        # normal cElementTree install
        import cElementTree as etree
        print("running with cElementTree")
      except ImportError:
        try:
          # normal ElementTree install
          import elementtree.ElementTree as etree
          print("running with ElementTree")
        except ImportError:
          print("Failed to import ElementTree from any known place")


class Builder(object):

    def __init__(self, write_in_file=True):
        self.xhtml_file = None
        if write_in_file:
            self.xhtml_file = open("xhtml.html", 'w')
            self.xhtml_file.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
            self.xhtml_file.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\""
                                  "\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">")

        self.root = etree.Element("html", xmls="http://www.w3.org/TR/xhtml1", lang="eng")
        self.root.append(etree.Element("head"))
        self.root[0].append(etree.Element("link", rel="stylesheet", type="text/css", href="style.css"))

        self.root[0].append(etree.Element("title"))
        self.root[0][-1].text = "Products information"
        self.root.append(etree.Element("body"))
        self.body = self.root[1]
        self.xhtml_file.write(etree.tostring(self.root[0], pretty_print=True))

    def add_new_product(self, page_link, price, descr_lines, img_souce):
        new_product = etree.Element("product")
        page_link_element = etree.Element("link").text = page_link
        price_element = etree.Element("price")
        # img_source_element = etree.Element(img_souce)

        table = etree.Element("table")
        table.append(etree.Element("td"))#cell for image in table
        table.append(etree.Element("td"))#cell for description-table of product
        table[0].append(etree.Element("img", src=img_souce))
        description_table = etree.Element("table")
        description_table.append(etree.Element("tr"))
        description_table[-1].append(etree.Element("td"))
        description_table[-1][-1].append(etree.Element("a", src=page_link))
        etree.SubElement(description_table, "tr")
        etree.SubElement(description_table[-1], "td")
        etree.SubElement(description_table[-1][-1], "p").text = "price"
        etree.SubElement(description_table[-1], "td")
        etree.SubElement(description_table[-1][-1], "p").text = price
        #
        # description_table.append(etree.Element("tr"))
        # description_table[-1].append(etree.Element("td"))
        # description_table[-1][-1].append(etree.Element("p"))
        # description_table[-1][-1][-1].text = "price"
        # description_table[-1].append(etree.Element("td"))
        # description_table[-1][-1].append(etree.Element("p"))
        # description_table[-1][-1][-1].text = price
        #
        for line in descr_lines:
            parent = etree.Element("tr")

            name = etree.Element("td")
            name.append(etree.Element("p"))
            name[-1].text = line[0][0].text

            capit = etree.Element("td")
            capit.append(etree.Element("p"))
            capit[-1].text = line[1][0].text

            parent.append(name)
            parent.append(capit)
            description_table.append(parent)

        table[1].append(description_table)

        self.body.append(table)
        # self.xhtml_file.write(etree.tostring(table, pretty_print=True))

    def write_in_file(self):
        self.xhtml_file.write(etree.tostring(self.body, pretty_print=True))

    def add_new_page(self, page_url):
        self.root.append(etree.Element("page", url=page_url))

    def add_fragment(self, content=None, attr="", name="fragment"):
        if name == "img":
            self.root.append(etree.Element(name, src=content))
            return
        self.root.append(etree.Element(name, type=attr))
        self.root[-1].text = content

    def get_tree(self, as_tree=True):
        return etree.tostring(self.root, pretty_print=as_tree)

